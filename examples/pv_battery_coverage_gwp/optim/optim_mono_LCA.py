#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from pv_battery_coverage_gwp.optim.optim_definition import OptimDefinition
myOpt = OptimDefinition(['gwp'])
result = myOpt.optim.run(ftol=1e-15, maxiter=500)
result.printResults()
# result.plotResults()


for name, value in result.rawResults.items():
    if not (('irradiance' in name) or ('consumption' in name)):
        print(name, '  \t =', value)

# Plot PV / Consumption
myOpt.plot()

